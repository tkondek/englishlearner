import java.awt.EventQueue;
import java.util.*;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
/**import javax.swing.text.Document;*/
import javax.swing.KeyStroke;

import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JTabbedPane;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;

import javax.swing.JPanel;
import java.awt.ComponentOrientation;
import java.awt.FlowLayout;
import java.io.*;
import java.text.SimpleDateFormat;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import org.w3c.dom.Attr;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerException;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.swing.JLabel;
import javax.swing.SwingConstants;



public class EnglishLearnerGui{

	private JFrame frame;
	private JTextField textFieldAddNewWord;
	private JTextField textFieldAddNewWordTransl;
	Map<String, String> dictionaryOfWords = new HashMap<String, String>();
	private JTextField textFieldPolishGame;
	private JTextField textFieldEnglishGame;
	private JTextField textFieldFindWord;
	private JLabel lblWordsLeftCounter;
	private JLabel lblWordsInDictionaryCounter;
	private ArrayList<ArrayList<String>> wordsList = new ArrayList<ArrayList<String>>();
	private ArrayList<ArrayList<String>> wordsListForGame = new ArrayList<ArrayList<String>>();
	private int randomWordIndex = 0;
	private int wordsAmount = 0;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					EnglishLearnerGui window = new EnglishLearnerGui();
					window.frame.setVisible(true);
 
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		
	}

	/**
	 * Create the application.
	 */
	public EnglishLearnerGui() {
		  initialize();
	}



	
	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		try {
			File xmlFile = new File("dictionary.xml");
			DocumentBuilderFactory dataFactory = DocumentBuilderFactory.newInstance();
		    DocumentBuilder documentBuilder = dataFactory.newDocumentBuilder();
			Document doc = documentBuilder.parse(xmlFile);
			
			//add to dictionaryOfWords all words from xml file
			NodeList nListElement = doc.getElementsByTagName("element");
			wordsAmount = nListElement.getLength();
			for (int temp=0; temp<nListElement.getLength(); temp++){
				ArrayList<String> words = new ArrayList<String>();
				Node nNode = nListElement.item(temp);
				Element eElement = (Element) nNode;
				//add to array list of words
				words.add(eElement.getAttribute("id"));
				words.add(eElement.getElementsByTagName("word").item(0).getTextContent());
				words.add(eElement.getElementsByTagName("translation").item(0).getTextContent());
				words.add(eElement.getAttribute("date"));
				wordsList.add(words);
				dictionaryOfWords.put(eElement.getElementsByTagName("word").item(0).getTextContent(), 
									  eElement.getElementsByTagName("translation").item(0).getTextContent());
				
			}
			System.out.println("wordsLIST" + wordsList);
		 
			for (String key : dictionaryOfWords.keySet()){
				System.out.println(key + " - " + dictionaryOfWords.get(key));
			}
 
		
		frame = new JFrame();
		frame.setBounds(100, 100, 500, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new BorderLayout(0, 0));
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		frame.getContentPane().add(tabbedPane);
		
		JPanel tabLearn = new JPanel();
		tabbedPane.addTab("Learn", null, tabLearn, null);
		tabLearn.setLayout(null);
		
		JLabel lblWordsLeftCounter = new JLabel("---");
		lblWordsLeftCounter.setBounds(251, 48, 46, 14);
		tabLearn.add(lblWordsLeftCounter);
		
		JButton btnStartGame = new JButton("Start Game");
		btnStartGame.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				wordsListForGame.clear();
				for(int i=0; i < wordsList.size(); i++ ){
					wordsListForGame.add(wordsList.get(i));
				}
				textFieldEnglishGame.setText("");
				lblWordsLeftCounter.setText(Integer.toString(wordsListForGame.size()-1));
				Random ran = new Random();
				int x = ran.nextInt(wordsListForGame.size());
				randomWordIndex = x;
				textFieldPolishGame.setText(wordsListForGame.get(x).get(1));
				textFieldEnglishGame.setToolTipText(wordsListForGame.get(x).get(2));
				textFieldEnglishGame.requestFocus();
				System.out.println(randomWordIndex);
			}

		});
		btnStartGame.setBounds(44, 55, 117, 23);
		tabLearn.add(btnStartGame);
		
		JButton btnFindWord = new JButton("Find Word");
		btnFindWord.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				findWord();
			}
		});
		

		btnFindWord.setBounds(219, 168, 89, 23);
		tabLearn.add(btnFindWord);
		
		JLabel lblPolishGame = new JLabel("Polish");
		lblPolishGame.setBounds(44, 11, 46, 14);
		tabLearn.add(lblPolishGame);
		
		JLabel lblEnglishGame = new JLabel("English");
		lblEnglishGame.setBounds(222, 11, 46, 14);
		tabLearn.add(lblEnglishGame);
		
		JLabel lblPolishenglishFind = new JLabel("Polish/English");
		lblPolishenglishFind.setBounds(44, 153, 89, 14);
		tabLearn.add(lblPolishenglishFind);
		
		textFieldPolishGame = new JTextField();
		lblPolishGame.setLabelFor(textFieldPolishGame);
		textFieldPolishGame.setBounds(44, 24, 133, 20);
		tabLearn.add(textFieldPolishGame);
		textFieldPolishGame.setColumns(10);
		
		textFieldEnglishGame = new JTextField();
		textFieldEnglishGame.setBounds(222, 24, 133, 20);
		tabLearn.add(textFieldEnglishGame);
		textFieldEnglishGame.setColumns(10);
		textFieldEnglishGame.addKeyListener(new KeyListener(){
				@Override
			    public void keyTyped(KeyEvent e) {
			    }

		        @Override
		        public void keyPressed(KeyEvent e) {
		        	if(e.getKeyCode() == 10){
		        		if(wordsListForGame.size() > 1){
							checkWord();
							lblWordsLeftCounter.setText(Integer.toString(wordsListForGame.size()-1));
						}
						else{
							textFieldEnglishGame.setText("");
							textFieldPolishGame.setText("");
							lblWordsLeftCounter.setText("---");
						}
		        	}
		        }
	
		        @Override
		        public void keyReleased(KeyEvent e) {
		        }
		});
		
		textFieldFindWord = new JTextField();
		textFieldFindWord.setBounds(44, 169, 133, 20);
		tabLearn.add(textFieldFindWord);
		textFieldFindWord.setColumns(10);
		textFieldFindWord.addKeyListener(new KeyListener(){
			@Override
		    public void keyTyped(KeyEvent e) {
		    }

	        @Override
	        public void keyPressed(KeyEvent e) {
	        	if(e.getKeyCode() == 10){
	        		findWord();
	        		}
	        }

	        @Override
	        public void keyReleased(KeyEvent e) {
	        }

		});
	
		
		JButton btnCheck = new JButton("Check");
		btnCheck.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(wordsListForGame.size() > 1){
					checkWord();
					lblWordsLeftCounter.setText(Integer.toString(wordsListForGame.size()-1));
				}
				else{
					textFieldEnglishGame.setText("");
					textFieldPolishGame.setText("");
					lblWordsLeftCounter.setText("---");
				}
			}
		});
	
		btnCheck.setBounds(364, 23, 81, 23);
		tabLearn.add(btnCheck);
		
		JLabel lblWordsInDistionary = new JLabel("words in dictionary: ");
		lblWordsInDistionary.setBounds(10, 209, 117, 14);
		tabLearn.add(lblWordsInDistionary);
		
		JLabel lblWordsInDictionaryCounter = new JLabel("");
		lblWordsInDictionaryCounter.setBounds(137, 209, 40, 14);
		lblWordsInDictionaryCounter.setText(Integer.toString(wordsAmount));
		tabLearn.add(lblWordsInDictionaryCounter);
		
		JLabel lblWordsLeft = new JLabel("Left:");
		lblWordsLeft.setBounds(222, 48, 32, 14);
		tabLearn.add(lblWordsLeft);
		
		JPanel tabAddNewWord = new JPanel();
		tabbedPane.addTab("Add New Word", null, tabAddNewWord, null);
		tabAddNewWord.setLayout(null);
		
		JButton btnAddToDict = new JButton("Add To Dictionary");
		btnAddToDict.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addWordToDictionary();
				lblWordsInDictionaryCounter.setText(Integer.toString(wordsList.size()));
				}
		});
		btnAddToDict.setBounds(104, 63, 169, 23);
		tabAddNewWord.add(btnAddToDict);
		
		textFieldAddNewWord = new JTextField();
		textFieldAddNewWord.setBounds(35, 32, 129, 20);
		tabAddNewWord.add(textFieldAddNewWord);
		textFieldAddNewWord.setColumns(10);
		
		textFieldAddNewWordTransl = new JTextField();
		textFieldAddNewWordTransl.setBounds(204, 32, 129, 20);
		tabAddNewWord.add(textFieldAddNewWordTransl);
		textFieldAddNewWordTransl.setColumns(10);
		textFieldAddNewWordTransl.addKeyListener(new KeyListener(){
			@Override
		    public void keyTyped(KeyEvent e) {
		    }

	        @Override
	        public void keyPressed(KeyEvent e) {
	        	if(e.getKeyCode() == 10){
	        		addWordToDictionary();
	        		lblWordsInDictionaryCounter.setText(Integer.toString(wordsList.size()));
	        		}
	        }

	        @Override
	        public void keyReleased(KeyEvent e) {
	        }
		});
		
		JLabel lblPolishAdd = new JLabel("Polish");
		lblPolishAdd.setBounds(35, 18, 46, 14);
		tabAddNewWord.add(lblPolishAdd);
		
		JLabel lblEnglishAdd = new JLabel("English");
		lblEnglishAdd.setBounds(204, 18, 46, 14);
		tabAddNewWord.add(lblEnglishAdd);
		
	
 
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public String findEnglishWord(){
		for(int i = 0; i < wordsList.size(); i++){
			if(wordsList.get(i).get(2).equals(textFieldFindWord.getText())){
				return wordsList.get(i).get(1);
			}
		}
		return "0";
	}
	
	public boolean saveDictToXml(){
		//write to xml file
		try {
			DocumentBuilderFactory dataFactory = DocumentBuilderFactory.newInstance();
		    DocumentBuilder documentBuilder = dataFactory.newDocumentBuilder();
			Document doc = documentBuilder.newDocument();
			//add root element
			Element rootElement = doc.createElement("words");
			doc.appendChild(rootElement);
			//add elements
			System.out.println(wordsList);
			for(int i = 0; i < wordsList.size(); i++){
				Element element = doc.createElement("element");
				rootElement.appendChild(element);
				element.setAttribute("id", wordsList.get(i).get(0));
				element.setAttribute("date", wordsList.get(i).get(3));
				
				Element word = doc.createElement("word");
				word.setTextContent(wordsList.get(i).get(1));
				element.appendChild(word);
				
				Element translation = doc.createElement("translation");
				translation.setTextContent(wordsList.get(i).get(2));
				element.appendChild(translation);
		 
			}
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File("dictionary.xml"));
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
			transformer.transform(source, result);
		} catch (TransformerException tfe) {
			tfe.printStackTrace();
		}catch (Exception e) {
			e.printStackTrace();
			}
			return true;
	}
	
	public void checkWord(){
		if(!textFieldEnglishGame.getText().isEmpty()){
			System.out.println(textFieldEnglishGame.getText().getClass() + " - " + wordsListForGame.get(randomWordIndex).get(2).getClass());
			if(textFieldEnglishGame.getText().equals(wordsListForGame.get(randomWordIndex).get(2))){
				System.out.println("Correct! " + randomWordIndex);
				wordsListForGame.remove(randomWordIndex);
				//get new word to translate automatically
				textFieldEnglishGame.setBackground(Color.WHITE);
				textFieldEnglishGame.setText("");

				if(wordsListForGame.size()>0){
					Random ran = new Random();
					int x = ran.nextInt(wordsListForGame.size());
					randomWordIndex = x;
					textFieldPolishGame.setText(wordsListForGame.get(x).get(1));
					textFieldEnglishGame.setToolTipText(wordsListForGame.get(x).get(2));
					textFieldEnglishGame.requestFocus();
					System.out.println(randomWordIndex);
				}
			} 
			 
			else{
				System.out.println("incorrect!! " + randomWordIndex);
				Color color = new Color(242,172,187);
				textFieldEnglishGame.setBackground(color);
				textFieldEnglishGame.requestFocus();
			}
		}
	}
	
	public void findWord(){
		if(textFieldFindWord.getText().isEmpty() == false){
			if(dictionaryOfWords.get(textFieldFindWord.getText()) != null){
				textFieldPolishGame.setText(textFieldFindWord.getText());
				textFieldEnglishGame.setText(dictionaryOfWords.get(textFieldFindWord.getText()));
			}
			else if(!findEnglishWord().equals("0")){
				textFieldEnglishGame.setText(textFieldFindWord.getText());
				textFieldPolishGame.setText(findEnglishWord());
			}
			else{
				System.out.println("nie znalazlem");
				textFieldPolishGame.setText("");
				textFieldEnglishGame.setText("");
			}
			
			System.out.println("Find: " + textFieldFindWord.getText() + " - " + dictionaryOfWords.get(textFieldFindWord.getText()));
		}
	}
	
	public void addWordToDictionary(){
		System.out.println("dupsko");
		if(!textFieldAddNewWord.getText().isEmpty() && !textFieldAddNewWordTransl.getText().isEmpty()){
			
			boolean existsInWordsList = false;
			for(int i=0; i<wordsList.size(); i++){
				if(wordsList.get(i).get(1).contains(textFieldAddNewWord.getText().replaceAll("\\s+",  ""))){
					existsInWordsList = true;
				};
				
			}
			if(existsInWordsList){
				JOptionPane.showMessageDialog(null, "'" + textFieldAddNewWord.getText().replaceAll("\\s+", "") + "'" + " - " + "already exists in dictionary!!");
				textFieldAddNewWord.setText("");
				textFieldAddNewWordTransl.setText("");
				textFieldAddNewWord.requestFocus();
			}
			//save new word to xml 
			else{
				//add words to dictionaryOfWords
				dictionaryOfWords.put(textFieldAddNewWord.getText(), textFieldAddNewWordTransl.getText());
				
				//add word to wordsList Array
				ArrayList<String> words = new ArrayList<String>();
				
				Calendar calendar = Calendar.getInstance();
				SimpleDateFormat dateOnly = new SimpleDateFormat("dd/MM/yyy");
				
				words.add(Integer.toString(wordsList.size()));
				words.add(textFieldAddNewWord.getText().replaceAll("\\s+", ""));
				words.add(textFieldAddNewWordTransl.getText().replaceAll("\\s+", ""));
				words.add(dateOnly.format(calendar.getTime()));
				
				wordsList.add(words);
				wordsListForGame.add(words);
				if(saveDictToXml()){
					JOptionPane.showMessageDialog(null, "'" + textFieldAddNewWord.getText().replaceAll("\\s+", "") + "'" + " - " + "saved correctly!!");
					textFieldAddNewWord.setText("");
					textFieldAddNewWordTransl.setText("");
					textFieldAddNewWord.requestFocus();
			
			};
		}
		}
	}
	private class WordsDictionary{
		String word = "";
		String translation = "";
		String id = "";
				
	}@Override
	protected Object clone() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		return super.clone();
	}
}
